module CrossTreePolycubesTest where

import CrossTreePolycubes
import Data.List

-- |Test the length invariant.
-- All isometries of a tree must have the same length (number of nodes).
-- 
-- >>> and [testIsometriesLen x | x <- [1..10]]
-- True
testIsometriesLen :: Int -> Bool
testIsometriesLen n = 1 == (length $ nub [treeLength t | t <- ts])   
    where ts = generateN n


-- |Test internals invariant.
-- Every unique polycube must have the same number of internal nodes.
--
-- >>> and [testPolycubeInternalLen x | x <- [1..10]]
-- True
testPolycubeInternalLen :: Int -> Bool
testPolycubeInternalLen n = all (==n) $ map nbInternals $ generateN n

-- |Test number of internals node invariant.
--
-- Every tree isometries must have the same number of internal node which is equal to n.
--
-- >>> and [testIsometriesInternalLen x | x <- [1..10]]
-- True
testIsometriesInternalLen ::  Int -> Bool
testIsometriesInternalLen n = all (==n) $ map nbInternals $ isometries $ head $ generateN n 

-- |Test number of isoemtries of a tree invariant
-- Every tree has 48 * (number of internal nodes) isometries.
--
-- >>> and [testIsometriesLen2 x | x <- [1..10]]
-- True
testIsometriesLen2 :: Int -> Bool
testIsometriesLen2 n = and [((==n').length) $ isometries x | x <- generateN n]
    where n' = 48*n


