import bpy
import json
from pprint import pprint

# code taken from http://blenderscripting.blogspot.ca/2012/03/deleting-objects-from-scene.html
candidates = [item.name for item in bpy.data.objects if item.type == "MESH"]
# select them only.
for object_name in candidates:
  bpy.data.objects[object_name].select = True

# remove all selected.
bpy.ops.object.delete()

#filename = "/Users/louis-vincentboudreault/ecole/stage/cross-tree-polycube/results/polycubes5.json"
filename = "/home/toothbrush/stage/polycube/results/polycubes5.json"

with open(filename) as f:
    data = json.load(f)

def add3(v1, v2):
    return (v1[0]+v2[0],v1[1]+v2[1],v1[2]+v2[2])

def max_x():
    m = -1
    for ele in data:
        cubes = [(cube[0],cube[1],cube[2]) for cube in ele["cubes"]]
        for cube in cubes:
            if m < cube[0]:
                m = cube[0]
    return m

offset = (0,0,0)
x_offset = max_x() + 4
for ele in data:
    cubes = [(cube[0],cube[1],cube[2]) for cube in ele["cubes"]]
    for cube in cubes:
        bpy.ops.mesh.primitive_cube_add(radius=0.5,location=add3(cube,offset))
    offset = add3(offset, (x_offset,0,0))
