{-# LANGUAGE OverloadedStrings #-}

module CrossTreePolycubes where

import Data.Ix
import Data.Array
import Data.List.Extra
import qualified Data.Set as Set
import Data.Maybe (fromJust)
import Control.Monad (guard)
import Control.Applicative
import Data.Aeson (toJSON, object, ToJSON, (.=))

------------------------
--- Utility Functions --
------------------------

-- |Same thing as indices, except it uses a super-supplied predicate to filter
-- unwanted indices.
indicesBy :: Ix i => ((i,e) -> Bool) -> Array i e -> [i]
indicesBy f arr = [i | i <- is, f (i, arr ! i)]
    where is = range $ bounds arr


-- |Act as null
data Nil = Nil deriving (Show,Eq)

data Axis = X | Y | Z deriving (Show,Eq,Enum)

data Unit
    = Xp
    | Xm
    | Yp
    | Ym
    | Zp
    | Zm
    deriving (Show, Ord, Bounded, Ix, Enum, Eq)

type Units = [Unit]
type Axes = [Axis]
newtype Vector = Vector [Int] deriving (Eq, Show, Ord)

instance Num Vector where
    Vector v1 + Vector v2 = Vector (zipWith (+) v1 v2)
    Vector v1 * Vector v2 = Vector (zipWith (*) v1 v2)
    Vector v1 - Vector v2 = Vector (zipWith (-) v1 v2)
    abs (Vector v) = Vector (map abs v)
    signum (Vector v) = Vector (map signum v)
    fromInteger i = Vector (repeat $ fromInteger i)

data Tree a
    = Empty
    | Leaf a
    | Internal a (Array Unit (Tree a))
    deriving (Show, Eq)

instance Functor Tree where
    fmap _ Empty = Empty
    fmap f (Leaf x) = Leaf (f x)
    fmap f (Internal x ts)  = Internal (f x) (fmap (fmap f) ts)  

instance Foldable Tree where
    foldMap _ Empty = mempty
    foldMap f (Leaf x) = f x
    foldMap f (Internal x ts) = foldl mappend (f x) [foldMap f t | t <- elems ts] 

instance Applicative Tree where
    pure = Leaf
    
    _ <*> Empty     = Empty
    Empty <*> _     = Empty

    (Leaf f) <*> (Leaf x)       = Leaf (f x)
    (Leaf f) <*> (Internal x _) = Leaf (f x)
    (Internal f _) <*> (Leaf x) = Leaf (f x)

    (Internal f fs) <*> (Internal x ts) = Internal (f x) $ listArray unitBounds
        [f' <*> t | u <- units, let f' = fs!u, let t  = ts!u]

instance Ord a => Ord (Tree a) where
    Empty `compare` Empty  = EQ
    Empty `compare` _ = LT
    _ `compare` Empty = GT
    (Leaf x) `compare` (Leaf y) = x `compare` y
    (Leaf x) `compare` (Internal y _) = if x == y then LT
                                        else x `compare` y
    (Internal x _) `compare` (Leaf y) = if x == y then GT 
                                        else x `compare` y
    (Internal x ts) `compare` (Internal y ts') =
        foldl mappend
              (x `compare` y)
              [tx `compare` ty | (tx,ty) <- zip (elems ts) (elems ts')]

-- |Breadcrump used to create a zipper for our (Tree a) 
data TCrumb a = Crumb Unit (Tree a) deriving Show
type TZipper a = (Tree a, [TCrumb a])

-- |Returns all the units
units :: Units
units = [Xp .. Zm]

-- |Returns all the axes
axes :: Axes
axes = [X .. Z]

-- |Array bounds represented by units.
unitBounds :: (Unit,Unit)
unitBounds = (minBound, maxBound)

-- |Returns the correspond axis of a unit
--
-- >>> axisFromUnit Xp
-- X
-- >>> axisFromUnit Xm
-- X
-- >>> axisFromUnit Yp
-- Y
-- >>> axisFromUnit Zm
-- Z
axisFromUnit :: Unit -> Axis
axisFromUnit u
    | u == Xp || u == Xm = X
    | u == Yp || u == Ym = Y
    | u == Zp || u == Zm = Z

unitsFromAxis :: Axis -> Units
unitsFromAxis X = [Xp,Xm]
unitsFromAxis Y = [Yp,Ym]
unitsFromAxis Z = [Zp,Zm]

complementUnit :: Unit -> Units
complementUnit = unitsFromAxis.axisFromUnit

-- |Returns the units represents by an array of axes.
--
-- precondition: The array of axis must contains only uniques.
unitsFromAxes :: Axes -> Units
unitsFromAxes = concatMap unitsFromAxis 

-- |Returns the opposite direction of a unit
--
-- >>> map opposite [Xp,Yp,Zp]
-- [Xm,Ym,Zm]
-- >>> map (opposite.opposite) [Xp,Yp,Zp]
-- [Xp,Yp,Zp]
opposite :: Unit -> Unit
opposite Xp = Xm
opposite Xm = Xp
opposite Yp = Ym
opposite Ym = Yp
opposite Zp = Zm
opposite Zm = Zp

-- |Returns the vector (0,0,0)
--
-- >>> zeroVector
-- Vector [0,0,0]
zeroVector :: Vector
zeroVector = Vector [0,0,0]

-- |Returns the vector representation of a unit.
--
-- >>> vectorFromUnit Xp
-- Vector [1,0,0]
-- >>> vectorFromUnit Zm
-- Vector [0,0,-1]
vectorFromUnit :: Unit -> Vector
vectorFromUnit Xp = Vector [1,0,0]
vectorFromUnit Xm = Vector [-1,0,0]
vectorFromUnit Yp = Vector [0,1,0]
vectorFromUnit Ym = Vector [0,-1,0]
vectorFromUnit Zp = Vector [0,0,1]
vectorFromUnit Zm = Vector [0,0,-1]

-- |Determines if a tree is Empty.
isEmpty :: Tree a -> Bool
isEmpty Empty = True
isEmpty _ = False

-- |Determines if a tree is a leaf.
isLeaf :: Tree a -> Bool
isLeaf (Leaf _) = True
isLeaf _ = False

-- |Determines if a tree is an internal node.
isInternal :: Tree a -> Bool
isInternal (Internal _ _) = True
isInternal _ = False

-- |Counts the number of internal nodes.
nbInternals :: Tree a -> Int
nbInternals t@(Internal _ ts) = 1 + sum [nbInternals t | t <- elems ts]
nbInternals _ = 0

longuestPath :: Tree a -> Int
longuestPath (Leaf _) = 1
longuestPath (Internal _ ts) = 1 + maximum [longuestPath t | t <- elems ts]
longuestPath _ = 0

treeLength :: Tree a -> Int
treeLength Empty = 0
treeLength (Leaf _) = 1
treeLength (Internal _ ts) = 1 + sum [treeLength t | t <- elems ts] 

-- |Determines the longest path between the root and the furthest internal node.
longuestPathInternal :: Tree a -> Int
longuestPathInternal (Internal _ ts) = 1 + maximum [longuestPathInternal t | t <- elems ts]
longuestPathInternal _ = 0

-- |Build a basic cross
--
-- >>> buildCross [Xp,Xm,Yp,Ym]
-- Internal Nil (array (Xp,Zm) [(Xp,Leaf Nil),(Xm,Leaf Nil),(Yp,Leaf Nil),(Ym,Leaf Nil),(Zp,Empty),(Zm,Empty)])
buildCross :: Units -> Tree Nil
buildCross us = Internal Nil 
    $ array unitBounds (zip units (repeat Empty))
        // zip us (repeat (Leaf Nil))


-- |Maps a tree structure to a tree structure whose value represents its coordonates in space.
toPolyCube :: Vector -> Tree a -> Tree Vector
toPolyCube _ Empty = Empty
toPolyCube v (Leaf _) = Leaf v
toPolyCube v (Internal _ ts) = Internal v $ listArray unitBounds es 
    where es = [toPolyCube v' e | (i,e) <- assocs ts, let v' = vectorFromUnit i + v]


-- |Collects all the cubes in space of a vector tree.
polyCubes :: Tree Vector -> [Vector]
polyCubes = foldr (:) [] 


-- |Selects the indices in a tree whose value are empty.
emptyUnits :: Tree a -> Units
emptyUnits Empty = []
emptyUnits (Leaf _) = units
emptyUnits (Internal _ ts) = indicesBy (\(i,e) -> isEmpty e) ts


-- |Calculates and collects all the vectors of all subtree `t` of `T` where `T` is the tree wrapped inside a zipper.
--
-- One important note on this function is it exclude the vector where the tree `T` comes from.
emptyExtension :: TZipper Vector -> [Vector]
emptyExtension (Empty, _) = []
emptyExtension (Leaf x, []) = [x + vectorFromUnit u' | u' <- units]  
emptyExtension (Leaf x, Crumb u _:_) = [x + vectorFromUnit u' | u' <- units, u' /= opposite u]  
emptyExtension (t@(Internal x ts), []) = [x + vectorFromUnit u' | u' <- emptyUnits t]
emptyExtension (t@(Internal x ts), Crumb u _:_)= [x + vectorFromUnit u' | u' <- emptyUnits t, u' /= opposite u] 


-- |A tree `T` and a set of cubes in space `S`, has a consistent neighborhood iff:
-- 
-- Forall subtrees `t` in `T` and forall empty nodes `e` (except the direction `t` comes from which is always empty) of `t`,
-- `e` is not element if `S`.
--
-- >>> let c1 = toPolyCube zeroVector $ buildCross [Xp,Xm,Yp,Ym]
-- >>> let s1 = Set.fromList $ polyCubes c1
-- >>> consistentNeighborhood c1 s1
-- True
consistentNeighborhood :: Tree Vector -> Set.Set Vector -> Bool
consistentNeighborhood root s = g' (root,[])
    where g' (Empty, _) = True
          g' z@(t@(Leaf _),_) = leafCond z
          g' z = intCond z
          leafCond z = and [v `Set.notMember` s | v <- emptyExtension z] 
          intCond  z@(t@(Internal _ ts), bs) = leafCond z &&
            and [g' (t', Crumb u t:bs) | u <- units, let t' = ts!u]


-- |Determines if a tree is geometrically acyclic
-- 
-- To be acyclic there must be only unique cubes in the space + no overlapping cubes.
isAcyclic :: Tree a -> Bool
isAcyclic t =  length cubes == Set.size cubesSet
            && consistentNeighborhood t' cubesSet
    where t' = toPolyCube zeroVector t
          cubes = polyCubes t'
          cubesSet = Set.fromList cubes


-- |Puts Nil to every value of a tree, leaving only its structure
nihilate :: Tree a -> Tree Nil
nihilate = fmap (const Nil)


-- |Generate all octants for three dimension in terms of functions.
-- The positive `+` symbol in an octant is represented by the `id` functions.
-- The first octant which is: (+,+,+) is expressed as (id,id,id).
-- Whereas (+,-,+) will be expressed as (id,neg,id).
--
-- octants :: (a -> a)      -- ^ The negation function
--         -> [[(a -> a)]]  -- ^ Permutations of positive and negation functions.
--
-- >>> length $ octants reverse
-- 8
octants :: (a -> a) -> [[a -> a]]
octants neg = sequence $ replicate 3 [id,neg]


-- |Generate all units arrangement permutations
--
-- >>> length $ unitsPermutations
-- 48
unitsPermutations :: [Units]
unitsPermutations = do
    as <- permutations axes
    octant <- octants reverse
    let temp = zipWith (.) octant (repeat unitsFromAxis)
    return $ concat $ zipWith ($) temp as

-- | Apply a structural permutation on a tree by applying a 
-- "signed permutation mask" on the current units arrangement.
--
-- You may want to recompute the elements value with the new structure.
applyPermutation :: Units -> Tree a -> Tree a
applyPermutation us t@(Internal x ts) = Internal x 
    $ ts // [(i, applyPermutation us $ ts!i') | (i, i') <- zip units us]
applyPermutation _ t = t


-- |Map every internal nodes to its TZipper equivalent
--
-- >>> let zs = internToZipper makeTestTree 
-- >>> length zs == nbInternals makeTestTree
-- True
-- >>> length $ filter isInternal $ map fst $ internToZipper makeTestTree
-- 5
-- >>> length $ internToZipper makeTestTree
-- 5
internToZipper :: Tree a -> [TZipper a]
internToZipper root = (root, []) : internToZipper' (root, [])
    where internToZipper' (t@(Internal _ ts), bs) = zs ++ concatMap internToZipper' zs
            where zs = [(t', Crumb u t:bs) | u <- units, 
                                             let t' = ts!u, 
                                             isInternal t']


-- |Raise an internal node to be the root of a tree
rootify :: TZipper a -> Tree a
rootify (t, []) = t 
rootify (tree@(Internal x xs), Crumb u root@(Internal y ys):bs) = Internal x xs'
    where u' = opposite u
          ys' = ys // [(u, Empty)] -- Modify one internal ahead
          xs' = xs // [(u', rootify (Internal y ys', bs))]

-- | Generates all the root permutations of a tree. 
rootPermutations :: Tree a -> [Tree a]
rootPermutations = map rootify . internToZipper


-- | Generates all the isometries of a tree.
--
-- Isometries can be found by applying signed permutations on the space axes and applying
-- root permutations on the tree (changing the root for another node).
isometries :: Tree a -> [Tree a]
isometries tree = do
    us <- unitsPermutations
    let tree' = applyPermutation us tree
    rootPermutations tree'


-- |Finds the representative of a tree.
-- 
-- The representative of a tree is equal to the smallest tree (lexicographically) of
-- all its isometries.
rep :: Tree a -> Tree Vector
rep t = minimum $ map (toPolyCube zeroVector) $ isometries t


-- |Returns units that correspond to an Empty subtree of a tree `T`.
leafUnits :: Tree a -> Units
leafUnits (Internal _ ts) = indicesBy (\(i,e) -> isLeaf e) ts
leafUnits _ = []

-- |Determines are compatible (at the local level)
-- Two trees T1 and T2 are compatible if they share at least one common axis
-- but not all axes.
compatible :: Tree a -> Tree a -> Bool
compatible t t' = us `intersect` us' /= [] && us /= us' 
    where us  = leafUnits t
          us' = leafUnits t' 


-- |Generate all possible grafts locally (node to node).
graft :: Tree a -> Tree a -> [Tree a]
graft t1@(Internal x ts) t2@(Internal x' ts') = do
    u <- units
    let u' = opposite u
    guard(isLeaf (ts!u))
    guard(isLeaf (ts'!u'))
    guard(compatible t1 t2)
    let t2' = Internal x' $ ts' // [(u', Empty)]
    let t1' = Internal x  $ ts  // [(u,  t2')]  
    return t1'
graft _ _ = []


-- |Generates all possible grafts on the entire tree.
-- 
-- This functions doesn't deal with overlapping nodes (in the geometric space), thus may need
-- more filtering for future comptutations.
grafts :: Tree a -> Tree a -> [Tree a]
grafts t1@(Internal x ts) t2@(Internal _ _) = do
    let gs = [(u, grafts t t2) | (u,t) <- assocs ts]
    graft t1 t2 ++ [Internal x $ ts//[(u,e)] | (u,es) <- gs, e <- es]
grafts _ _ = []


-- |All the possible crosses. (constant function)
crosses :: [Tree Nil]
crosses = nub [buildCross $ unitsFromAxes [a1,a2] | a1 <- axes, a2 <- axes, a1 /= a2]

-- | Generates all unique polycubes assembly for n kernel
--
-- >>> length $ generateN 2
-- 1
-- >>> length $ generateN 3
-- 2
-- >>> length $ generateN 4
-- 3
-- >>> length $ generateN 5
-- 8
generateN :: Int -> [Tree Nil]
generateN = (map generateN' [0..] !!)
    where generateN' 0 = []
          generateN' 1 = [nihilate $ rep $ buildCross [Xp,Xm,Yp,Ym]]
          generateN' n = nubOrdBy cmpTreeRep [g | f <- generateN (n-1), c <- crosses, g <- grafts f c, isAcyclic g] 

-- |Determines if a two trees are equals iff they have the same representative.
cmpTreeRep :: Tree a -> Tree a -> Ordering
cmpTreeRep t1 t2 = rep t1 `compare` rep t2


instance ToJSON Vector where
    toJSON (Vector vs) = toJSON vs


instance ToJSON (Tree a) where
    toJSON t = 
        object ["cubes" .= cubes]
        where cubes = polyCubes $ toPolyCube zeroVector t


-- |Tree only used for testing purposes
makeTestTree :: Tree Nil
makeTestTree = 
    Internal Nil $ listArray unitBounds
        [
            Internal Nil $ listArray unitBounds
                [
                    Internal Nil $ listArray unitBounds [Leaf Nil, Empty, Leaf Nil, Leaf Nil, Empty, Empty],
                    Empty,
                    Empty,
                    Empty,
                    Leaf Nil,
                    Leaf Nil
                ]
            ,
            Leaf Nil,
            Leaf Nil,
            Internal Nil $ listArray unitBounds
                [
                    Empty,
                    Empty,
                    Empty,
                    Internal Nil $ listArray unitBounds
                        [
                            Leaf Nil,
                            Leaf Nil,
                            Empty,
                            Leaf Nil,
                            Empty,
                            Empty
                        ]
                    ,
                    Leaf Nil,
                    Leaf Nil
                ]
            ,
            Empty,
            Empty
        ]

-- |Tree only used for testing purposes
makeTestTree2 :: Tree Int
makeTestTree2 = Internal 1 $ listArray unitBounds
        [
            Internal 2 $ listArray unitBounds
                [
                    Internal 3 $ listArray unitBounds [Leaf 4, Empty, Leaf 5, Leaf 6, Empty, Empty],
                    Empty,
                    Empty,
                    Empty,
                    Leaf 7,
                    Leaf 8
                ]
            ,
            Leaf 9,
            Leaf 10,
            Internal 11 $ listArray unitBounds
                [
                    Empty,
                    Empty,
                    Empty,
                    Internal 12 $ listArray unitBounds
                        [
                            Leaf 13,
                            Leaf 14,
                            Empty,
                            Leaf 15,
                            Empty,
                            Empty
                        ]
                    ,
                    Leaf 16,
                    Leaf 17
                ]
            ,
            Empty,
            Empty
        ]

