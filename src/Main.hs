import CrossTreePolycubes
import System.Environment
import Control.Monad
import Data.Aeson.Encode.Pretty (encodePretty)
import Data.Aeson (toJSON)
import qualified Data.ByteString.Lazy as BL

main = do
    forM_ [0..15] $ \n -> do
        putStrLn $ "Computing cross-tree-polycube for " ++ show n ++ " crosses..."
        --let filename = "tmp/polycubes" ++ show n ++ ".json"
        let forms = generateN n
        --BL.writeFile filename $ encodePretty $ toJSON forms
        putStrLn $ "Number of uniq forms found: " ++ show (length forms) ++ "\n"
