# Project Title

This repository contains haskell code to generate uniques cross-tree polycube arrangement. This project was
realised to be in conjunction with a more general problem: [Fully leafed tree-like polycubes](https://gitlab.com/ablondin/flis-tree-polycubes).

## Dependencies

- [Stack](https://docs.haskellstack.org/en/stable/README/#how-to-install), Build tool for haskell project, used to create portable solution and isolated development environment;
- [Cabal](https://www.haskell.org/cabal/download.html), used as the package manager for Stack;
- [Blender Latest 2.79 build](https://www.blender.org) (optional), to generate all the polycubes in a scene;
- [Doctest](https://github.com/sol/doctest) (optional), to execute the test inside all docstrings.

## Build and run

To build the project, execute the following command: 

```bash
$ make build
```

To run the project:
```bash
$ make run
```

If you need to recompile from scratch you can clean your project with this command:
```bash
$ make clean
```
This will remove all binaries and temporary files created by the execution or build process.

## Running the tests

If you installed [Doctest](https://github.com/sol/doctest), you can execute the command:
```bash
$ make test
```

## Authors

Louis-Vincent Boudreault

## License

This project is licensed under the GPL-3 License - see the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments
- [README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2#file-readme-template-md);
- [Tree-Polycubes](https://gitlab.com/ablondin/flis-tree-polycubes).
