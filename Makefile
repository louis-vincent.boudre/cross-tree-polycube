EXEC=cross-tree-polycube
.PHONY: clean compile install run build brun

build: 
	stack build
run:
	stack exec $(EXEC)

brun: build run

test:
	doctest src/CrossTreePolycubes.hs

clean:
	rm -f polycubes*
	rm -fr tmp/*
	rm -fr .stack-work/
